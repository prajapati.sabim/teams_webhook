<?php
namespace Sabim\TeamsWebhook;


use Monolog\Logger;
use Sabim\TeamsWebhook\Handler\MicrosoftTeamsHandler;

class LogMonolog
{
    /**
     * @param array $config
     * @return Logger
     */
    public function __invoke(array $config)
    {
        return new Logger(
            $config['title'],
            [new MicrosoftTeamsHandler(
                    $config['webhookDsn'],
                    $config['level'],
                    $config['title'],
                    $config['subject'],
                    $config['emoji'],
                    $config['color'],
                    $config['format']
                )
            ]
        );
    }
}
