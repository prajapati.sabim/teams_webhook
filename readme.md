#### logging.php

Add New Channel

```sh
'teams' => [
    'driver' => 'custom',
    'via'    => \Sabim\TeamsWebhook\LogMonolog::class,//#2
    'webhookDsn' => env('ACTIVED_MS_TEAMS_DSN'),
    'level'  => env('LOG_LEVEL', 'debug'), // or simply 'debug'
    'title'  => 'Server Error', // can be NULL
    'subject'  => '', // can be NULL
    'emoji'  => '&#x1f613', // can be NULL
    'color'  => '#fd0404', // can be NULL
    'format' => '[%datetime%] '.env('APP_NAME', 'Laravel').' '.env('APP_URL', 'localhost').' %channel%.%level_name% : %message%' // can be NULL
],
'stack' => [
    'driver' => 'stack',
    'channels' => ['daily','slack','custom'],
    'ignore_exceptions' => false,
],
```
